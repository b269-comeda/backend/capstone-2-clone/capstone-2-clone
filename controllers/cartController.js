const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");

// adding multiple products to a cart without creating a new one
module.exports.userCheckoutSingle = async (userId, isAdmin, productId, quantity) => {
  if (isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    try {
      let cart = await Cart.findOne({ userId });
      const selectedProduct = await Product.findById(productId);
      const amountingPrice = selectedProduct.price * quantity;
      
      if (!cart) {
        // If cart doesn't exist, create a new one
        cart = new Cart({
          userId,
          products: [{
            productId: selectedProduct._id,
            name: selectedProduct.name,
            description: selectedProduct.description,
            price: selectedProduct.price,
            quantity,
            subTotal: amountingPrice
          }],
          totalAmount: amountingPrice
        });
      } else if (quantity > 0) {
        // If cart exists and quantity is greater than zero, update it with the new product details
        const index = cart.products.findIndex(product => product.productId.toString() === productId);

        if (index !== -1) {
          // If the product already exists in the cart, update the quantity and subTotal
          cart.products[index].quantity += Number(quantity);
          cart.products[index].subTotal = cart.products[index].quantity * cart.products[index].price;
        } else {
          // If the product doesn't exist in the cart, add it to the products array
          cart.products.push({
            productId: selectedProduct._id,
            name: selectedProduct.name,
            description: selectedProduct.description,
            price: selectedProduct.price,
            quantity: Number(quantity),
            subTotal: amountingPrice
          });
        }
        // Update the totalAmount of the cart
        cart.totalAmount = cart.products.reduce((total, product) => total + product.subTotal, 0);
      } else {
        // If cart exists and quantity is zero, empty the cart
        cart.products = [];
        cart.totalAmount = 0;
      }

      await cart.save();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
};

// removing ALL PRODUCTS from an existing cart
module.exports.emptyCartByUserId = async (userId, isAdmin) => {
  if (isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    try {
      const cart = await Cart.findOne({ userId });

      if (!cart) {
        return false; //Promise.resolve({ value: `Cart is already empty.` });
      } else {
        cart.products = [];
        cart.totalAmount = 0;

        await cart.save();
        return true; //Promise.resolve({ value: `Cart has been emptied successfully.` });
      }
    } catch (error) {
      console.log(error);
      return Promise.reject(error);
    }
  }
};


// removing a single product from an existing cart
module.exports.removeProductFromCart = async (userId, isAdmin, productId) => {
	if (isAdmin) {
		return Promise.resolve({value: `This feature is not available to you.`});
	} else {
		try {
			// Find the cart for the user
			let cart = await Cart.findOne({ userId });
			
			// If cart doesn't exist, return an error message
			if (!cart) {
				return Promise.resolve({value: `Cart not found.`});
			} else { // If cart exists, remove the product with the specified ID from the products array
				const index = cart.products.findIndex(product => product.productId.toString() === productId);
				
				if (index !== -1) { // If the product exists in the cart, remove it from the array and update the totalAmount of the cart
					const removedProduct = cart.products.splice(index, 1);
					cart.totalAmount -= removedProduct[0].subTotal;
					
					return cart.save().then(() => {
						return true;
					});
				} else { // If the product doesn't exist in the cart, return an error message
					return Promise.resolve({value: `Product not found in cart.`});
				}
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	}
};

// updating a product quantity on an existing cart
module.exports.updateCartQuantity = async (userId, isAdmin, productId, newQuantity) => {
	if (isAdmin) {
		return Promise.resolve({value: `This feature is not available to you.`});
	} else {
		try {
			// Find the cart for the user
			let cart = await Cart.findOne({ userId });
			
			// If cart doesn't exist, return an error message
			if (!cart) {
				return Promise.resolve({value: `Cart not found for user.`});
			} else { // If cart exists, update the quantity of the product
				const index = cart.products.findIndex(product => product.productId.toString() === productId);
				if (index !== -1) { // If the product exists in the cart, update the quantity and subTotal
					cart.products[index].quantity = newQuantity;
					cart.products[index].subTotal = cart.products[index].quantity * cart.products[index].price;
					
					// Update the totalAmount of the cart
					cart.totalAmount = cart.products.reduce((total, product) => total + product.subTotal, 0);
					
					// Save the updated cart
					await cart.save();
					
					return true;
				} else { // If the product doesn't exist in the cart, return an error message
					return Promise.resolve({value: `Product not found in cart.`});
				}
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	}
};

/*===============================================================*/
// adding a single product in cart 
/*module.exports.userCheckoutSingle = async (data) => {
	if (data.isAdmin) {
		return Promise.resolve({value: `This feature is not available to you.`});
	} else {
		const selectedProduct = await Product.findById({_id: data.productId});
		const amountingPrice = selectedProduct.price * data.quantity;
		console.log(selectedProduct._id)
		const newCart = new Cart({
			userId: data.userId,
			products: [{
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: data.quantity,
				subTotal: amountingPrice
			}],
			totalAmount: amountingPrice
		});

		return newCart.save().then((order, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	}

};*/

// adding multiple products to cart (CREATING A CART)
module.exports.addToCart = async (data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		console.log(productsArray);
		const productSpec = [];
		let newCart = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct; // accumulate the total
			
			newCart.products.push(pushToProductsArray);
		}

		newCart = new Cart({
			userId: data.userId,
			products: newCart.products,
			totalAmount: currentTotalAmount
		});

		const cart = await newCart.save();

		if (cart) {
			// const cartCreated = `Cart successfully created! The current total amount of the item(s) in your cart is ${currentTotalAmount}.`;
			// return cartCreated;
			return true;
		} else {
			return false;
		}
	}
};

/*==========[STRETCH GOALS]=============*/

// retrieve authenticated user's cart
module.exports.retrieveCart = (reqParams) => {
	return Cart.findById(reqParams.cartId).then(result => {
		return result;
	});
};

// ++++++++++++++++++++++++++++++++++++
/*
module.exports.findCartByUserId = (userId) => {
	 try {
    const cart = Cart.findOne({ userId });
    if (!cart) {
      return { error: `Cart not found for user with ID ${userId}` };
    }
    return cart;
  } catch (error) {
    throw error;
  }
};
*/
module.exports.findCartByUserId = (data) => {
	return Cart.find({userId: data.userId}).then(result => {
		return result;
	});
};
// ++++++++++++++++++++++++++++++++++++

// retrieve ALL carts (ADMIN only)
module.exports.retrieveAllCart = (data) => {
	if (data.isAdmin) {
		return Cart.find({}).then(result => {return result; });
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return value;
	});
};

// change product's quantity inside a cart || add/remove product(s)
module.exports.changeQuantity = async (reqParams, data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		const productSpec = [];
		let newQuantity = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct; // accumulate the total
			newQuantity = {
				userId: data.userId,
				products: productSpec,
				totalAmount: currentTotalAmount
			};

			newQuantity.products.push(pushToProductsArray);
		}

		return Cart.findByIdAndUpdate(reqParams.cartId, newQuantity).then((cart, error) => {
			if (error) {
				return false;
			} else {
				let cartUpdated = `The cart has been successfully updated. You new current total is ₱${currentTotalAmount}.`
				return cartUpdated;
			};
		});
	}
};


