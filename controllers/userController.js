const User = require("../models/User");
const Product = require("../models/Product"); 
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};

// to register a user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			// let successfulRegistrationMessage = `${reqBody.email} is now registered!`
			// return successfulRegistrationMessage;
			return true;
		};
	});
};

// log in for registered user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			// let noEmailMessage = `Please input a registered email.`
			// return noEmailMessage;
			return false;
		} else if (reqBody.password == null) {
			// let noPasswordIncluded = `Password required!`
			// return noPasswordIncluded;
			return false;
		} else {
			const isPasswordRight = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordRight) {
				return {access: auth.createAccessToken(result)};
			} else {
				// let errorMessage = `Password is incorrect!`
				// return errorMessage;
				return false;
			};
		};
	});
};

// retrieving user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	});
};

// ----------------------[SECTION] STRETCH GOALS----------------------
// setting user to admin (ADMIN only)
module.exports.setUserToAdmin = (reqParams, data) => {
	if (data.isAdmin) {
		let userToAdmin = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(reqParams.userId, userToAdmin).then((user, error) => {
			if (error) {
				return false;
			} else {
				let requestAccepted = `User successfully updated to Admin.`;
				return requestAccepted;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
}

// to change a user's password
module.exports.changePassword = (email, newPassword) => {
    return User.findOne({ email: email }).then((user) => {
        if (!user) {
            return false;
        } else {
            user.password = bcrypt.hashSync(newPassword, 10);
            return user.save().then((user, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            });
        }
    });
};
