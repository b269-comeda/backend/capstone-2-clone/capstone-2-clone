const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const Cart = require("../models/Cart");


// user checkout (creating order)
module.exports.createOrder = async (data) => {
  if (data.isAdmin) {
    return Promise.resolve({ value: `This feature is not available to you.` });
  } else {
    const selectedCart = await Cart.findById({ _id: data.cartId });
    console.log(selectedCart);

    const productSpecs = selectedCart.products;
    console.log(productSpecs);

    const newOrder = new Order({
      userId: selectedCart.userId,
      cartId: data.cartId,
      products: productSpecs,
      totalAmount: selectedCart.totalAmount,
    });

    // Empty the cart and set totalAmount to 0
    selectedCart.products = [];
    selectedCart.totalAmount = 0;
    await selectedCart.save();

    return newOrder.save().then((order, error) => {
      if (error) {
        return false;
      } else {
        // let orderCreated = `Order successfully created. Total amount to be paid is ₱${selectedCart.totalAmount}.`;
        // return orderCreated;
        return true;
      }
    });
  }
};


/*==========[STRETCH GOALS]=============*/

// retrieve authenticated user's order
module.exports.retrieveOrder = (reqParams) => {
	return Order.findById(reqParams.orderId).then(result => {
		return result;
	});
};

// retrieve authenticated user's order [FINAL]
module.exports.findOrderByUserId = (data) => {
	return Order.find({userId: data.userId}).then(result => {
		return result;
	});
};

// retrieve ALL orders (ADMIN only)
module.exports.retrieveAllOrder = (data) => {
	if (data.isAdmin) {
		return Order.find({}).then(result => {return result; });
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return value;
	});
};




