const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// route for creating new products for ADMIN only
router.post("/create-product", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// route for getting all active products
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

// route for retrieving a specific product
router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// route for updating a specific product (ADMIN only)
router.put("/update/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProductInfo(req.params, data).then(resultFromController => res.send(resultFromController));
});

// route for archiving products (ADMIN only)
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

// route for reactivating products (ADMIN only)
router.patch("/reactivate/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.reactivateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

// route for getting ALL products (ADMIN only)
router.get("/all/inventory", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.retrieveAllProducts(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;