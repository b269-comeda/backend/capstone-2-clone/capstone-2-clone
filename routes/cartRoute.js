const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const cartController = require("../controllers/cartController");

const auth = require("../auth");

// route for adding multiple products to a cart without creating a new one
router.post("/checkout-single-product", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.userCheckoutSingle(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

// removing ALL PRODUCTS from a cart
router.delete("/empty-cart", auth.verify, async (req, res) => {
  try {
    const userId = auth.decode(req.headers.authorization).id;
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    const resultFromController = await cartController.emptyCartByUserId(userId, isAdmin);
    res.send(resultFromController);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

// route for removing single product from an existing cart
router.post("/remove-product", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.removeProductFromCart(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

// updating a product quantity on an existing cart
router.post("/amount-to-remove", auth.verify, async (req, res) => {
	try {
		const userId = auth.decode(req.headers.authorization).id;
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		const productId = req.body.productId;
		const quantity = req.body.quantity;
		
		const resultFromController = await cartController.updateCartQuantity(userId, isAdmin, productId, quantity);
		res.send(resultFromController);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

/*===============================================================*/
/*// route for adding a single product in cart 
router.post("/checkout-single-product", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	cartController.userCheckoutSingle(data).then(resultFromController => res.send(resultFromController));
});*/


// route for adding multiple products to cart (CREATING A CART)
router.post("/add-to-cart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});


/*==========[STRETCH GOALS]=============*/


// route for retrieving authenticated user's cart 
router.get("/view-cart/:cartId", (req, res) => {
	cartController.retrieveCart(req.params).then(resultFromController => res.send(resultFromController));
});

// ++++++++++++++++++++++++++++++++++++
/*
router.get('/cart/:userId', async (req, res) => {
  const { userId } = req.params;
  try {
    const cart = await cartController.findCartByUserId(userId);
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});
*/
router.get('/view/userscart', (req, res) => {
  const data = {
  	userId: auth.decode(req.headers.authorization).id,
  }
  cartController.findCartByUserId(data).then(resultFromController => res.send(resultFromController));
});
// ++++++++++++++++++++++++++++++++++++

// route for retrieving ALL carts (ADMIN only)
router.get("/all/createdcarts", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.retrieveAllCart(data).then(resultFromController => res.send(resultFromController));
});

// route for changing the product's quantity inside a cart || add/remove product(s)
router.patch("/change-quantity/:cartId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.changeQuantity(req.params, data).then(resultFromController => res.send(resultFromController));
});

module.exports = router; // PUT YOUR CODE ABOVE THIS LINE, JEH



